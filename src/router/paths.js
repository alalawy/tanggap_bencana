import Guard from '../commons/middleware';
export default [

  {
    path: '*',
    meta: {
      public: true,
    },
    redirect: {
      path: '/404'
    }
  },  
  {
    path: '/404',
    meta: {
      public: true,
    },
    name: 'NotFound',
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/NotFound.vue`
    )
  },
  {
    path: '/403',
    meta: {
      public: true,
    },
    name: 'AccessDenied',
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/Deny.vue`
    )
  },
  {
    path: '/500',
    meta: {
      public: true,
    },
    name: 'ServerError',
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/Error.vue`
    )
  },
  {
    path: '/login',
    meta: {
      public: true,
    },
    beforeEnter: Guard.guest,
    name: 'Login',
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/Login.vue`
    )
  },
  {
    path: '/confirm-phone',
    meta: {
      public: true,
    },
    name: 'ConfirmPhone',
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/ConfirmPhone.vue`
    )
  },
  {
    path: '/',
    meta: { },
    name: 'Root',
    redirect: {
      name: 'Login'
    }
  },
  {
    path: '/admin',
    meta: { },
    name: 'AdminRoot',
    beforeEnter: Guard.auth,
    redirect: {
      name: 'Dashboard'
    }
  },
  {
    path: '/admin/dashboard',
    meta: { breadcrumb: true },
    name: 'Dashboard',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/Dashboard.vue`
    )
  },
  {
    path: '/admin/data/posko_ungsi',
    meta: { breadcrumb: true },
    name: 'data/posko_ungsi',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/data/PoskoUngsi.vue`
    )
  },
  {
    path: '/admin/data/posko_bantuan',
    meta: { breadcrumb: true },
    name: 'data/posko_bantuan',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/data/PoskoBantuan.vue`
    )
  },
  {
    path: '/admin/data/fasilitas_umum',
    meta: { breadcrumb: true },
    name: 'data/fasilitas_umum',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/data/FasilitasUmum.vue`
    )
  },
  {
    path: '/admin/data/fasilitas_ibadah',
    meta: { breadcrumb: true },
    name: 'data/fasilitas_ibadah',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/data/FasilitasIbadah.vue`
    )
  },
  {
    path: '/admin/data/fasilitas_kesehatan',
    meta: { breadcrumb: true },
    name: 'data/fasilitas_kesehatan',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/data/FasilitasKesehatan.vue`
    )
  },
  {
    path: '/admin/data/fasilitas_pendidikan',
    meta: { breadcrumb: true },
    name: 'data/fasilitas_pendidikan',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/data/FasilitasPendidikan.vue`
    )
  },
  {
    path: '/admin/data/infrastruktur',
    meta: { breadcrumb: true },
    name: 'data/infrastruktur',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/data/Infrastruktur.vue`
    )
  },
  {
    path: '/admin/data/rumah_rusak',
    meta: { breadcrumb: true },
    name: 'data/rumah_rusak',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/data/RumahRusak.vue`
    )
  },
  {
    path: '/admin/features/emergency_calls',
    meta: { breadcrumb: true },
    name: 'features/emergency_calls',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/features/Emergency.vue`
    )
  },
  {
    path: '/admin/person/relawan',
    meta: { breadcrumb: true },
    name: 'person/relawan',
    beforeEnter: Guard.auth,
    component: () => import(
      /* webpackChunkName: 'routes' */
      /* webpackMode: 'lazy-once' */
      `@/pages/person/DataRelawan.vue`
    )
  },
];
