import { firestore } from '../commons/firebase_common';

export default {
  namespaced: true,
  state: {
    faskess: []
  },
  getters: {
    getFaskess (state) {
      return state.faskess;
    }
  },
  mutations: {
    setFaskess (state, faskes_obj) {
      state.faskess.push(faskes_obj);
    },
    addFaskes (state, faskes_obj) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('faskes')
        .collection('list')
        .add(faskes_obj).then(res => {
          console.log(res);
        }).catch(error => {
          console.log(error);
        });
    },
    clearFaskess (state) {
      state.faskess = [];
    },
    fetchFaskess (state) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('faskes')
        .collection('list')
        .onSnapshot(doc => {
          state.faskess = [];
          for (let i = 0; i < doc.docs.length; i++) {
            state.faskess.push(doc.docs[i].data());
          }
        });
    }
  },
  actions: {
    fetchFaskess: ({ commit }) => {
      commit('fetchFaskess');
    },
    addFaskes: ({ commit }, faskes_obj) => {
      commit('addFaskes', faskes_obj);
    },
    setFaskess: ({ commit }, faskes_obj) => {
      commit('setFaskess', faskes_obj);
    },
    clearFaskess: ({ commit }) => {
      commit('clearFaskess');
    }
  }
};
