import { firestore } from '../commons/firebase_common';

export default {
  namespaced: true,
  state: {
    fasdiks: []
  },
  getters: {
    getFasdiks (state) {
      return state.fasdiks;
    }
  },
  mutations: {
    setFasdiks (state, fasdik_obj) {
      state.fasdiks.push(fasdik_obj);
    },
    addFasdik (state, fasdik_obj) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('fasdik')
        .collection('list')
        .add(fasdik_obj).then(res => {
          console.log(res);
        }).catch(error => {
          console.log(error);
        });
    },
    clearFasdiks (state) {
      state.fasdiks = [];
    },
    fetchFasdiks (state) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('fasdik')
        .collection('list')
        .onSnapshot(doc => {
          state.fasdiks = [];
          for (let i = 0; i < doc.docs.length; i++) {
            state.fasdiks.push(doc.docs[i].data());
          }
        });
    }
  },
  actions: {
    fetchFasdiks: ({ commit }) => {
      commit('fetchFasdiks');
    },
    addFasdik: ({ commit }, fasdik_obj) => {
      commit('addFasdik', fasdik_obj);
    },
    setFasdiks: ({ commit }, fasdik_obj) => {
      commit('setFasdiks', fasdik_obj);
    },
    clearFasdiks: ({ commit }) => {
      commit('clearFasdiks');
    }
  }
};
