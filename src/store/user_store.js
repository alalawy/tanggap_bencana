import { firestore } from '../commons/firebase_common';

export default {
  namespaced: true,
  state: {
    users: [],
    user: null
  },
  getters: {
    getUsers (state) {
      return state.users;
    }
  },
  mutations: {
    setUsers (state, user) {
      state.users.push(user);
    },
    addUser (state, user) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('users')
        .collection('list')
        .add(user)
        .then(res => {
          console.log(res);
        })
        .catch(error => {
          console.log(error);
        });
    },
    clearUsers (state) {
      state.users = [];
    },
    fetchUsers (state) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('users')
        .collection('list')
        .onSnapshot(doc => {
          state.users = [];
          for (let i = 0; i < doc.docs.length; i++) {
            state.users.push(doc.docs[i].data());
          }
        });
    }
  },
  actions: {

    fetchUsers: ({ commit }) => {
      commit('fetchUsers');
    },
    addUser: ({ commit }, user) => {
      commit('addUser', user);
    },
    setUsers: ({ commit }, user) => {
      commit('setUsers', user);
    },
    clearUsers: ({ commit }) => {
      commit('clearUsers');
    }
  }
};
