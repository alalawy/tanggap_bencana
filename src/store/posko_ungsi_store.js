import { firestore } from '../commons/firebase_common';

export default {
  namespaced: true,
  state: {
    poskos: [],
    posko: null
  },
  getters: {
    getPoskos (state) {
      return state.poskos;
    }
  },
  mutations: {
    setPoskos (state, posko) {
      state.poskos.push(posko);
    },
    addPosko (state, posko) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('posko')
        .collection('list')
        .add(posko).then(res => {
          console.log(res);
        }).catch(error => {
          console.log(error);
        });
    },
    clearPoskos (state) {
      state.poskos = [];
    },
    fetchPoskos (state) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('posko')
        .collection('list')
        .onSnapshot(doc => {
          state.poskos = [];
          for (let i = 0; i < doc.docs.length; i++) {
            state.poskos.push(doc.docs[i].data());
          }
        });
    }
  },
  actions: {
    fetchPoskos: ({ commit }) => {
      commit('fetchPoskos');
    },
    addPosko: ({ commit }, posko) => {
      commit('addPosko', posko);
    },
    setPoskos: ({ commit }, posko) => {
      commit('setPoskos', posko);
    },
    clearPoskos: ({ commit }) => {
      commit('clearPoskos');
    }
  }
};
