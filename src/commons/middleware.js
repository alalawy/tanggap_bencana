import { getToken, getPhone } from './get_token';

export default {
  guest (to, from, next) {
    next(!getToken());
  },

  confirmPhone (to, from, next) {
    next(!getPhone());
  },

  auth (to, from, next) {
    next(
      getToken()
        ? true
        : {
          path: '/login',
          query: {
            redirect: to.name
          }
        }
    );
  }
};
