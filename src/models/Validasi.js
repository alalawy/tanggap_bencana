const STATUS_UNVERIFIED = 0;
const STATUS_VERIFIED = 1;
const STATUS_CLOSED = 2;
class Validasi {
  constructor () {
    this.intStatus = STATUS_UNVERIFIED; 
    this.strValidator = null;
    this.lngTimestamp = -1;
  }
}

export {
  STATUS_UNVERIFIED,
  STATUS_CLOSED,
  STATUS_VERIFIED,
  Validasi
};
