import { Validasi } from './Validasi';
import LokasiItem from './LokasiItem';

export default class PoskoPengungsian extends LokasiItem {
  constructor (namaPosko, pjNama, pJKontak) {
    super();
    this.strNamaPosko = namaPosko;
    this.strPJNama = pjNama;
    this.strPJKontak = pJKontak;
    this.intJumlahKK = 0;
    this.intJumlahJiwa = 0;
    this.intJumlahBayiNBalita = 0;
    this.intJumlahAnak = 0;
    this.intJumlahLansia = 0;
    this.intJumlahSakitRawatInap = 0;
    this.intJumlahSakit = 0;
    this.intJumlahBumil = 0;
    this.strKeterangan = null;
    this.strAddressName = null;
    this.strPelaporID = null;
    this.deleted = false;
    this.validasi = null;

    this.intJumlahCacatL = 0;
    this.intJumlahCacatP = 0;
    this.intJumlahMeninggal = 0;
    this.intJumlahPulang = 0;
  }
}
