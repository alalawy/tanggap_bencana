import { Validasi } from './Validasi';

export default class User {
  constructor (name, nik, email) {
    this.uid = null;
    this.name = null;
    this.nik = null;
    this.email = null;
    this.age = null;
    this.address = null;
    this.userType = 'REGULAR';
    this.lembaga = null;
    this.userRole = 'RELAWAN';
    this.phone = null;
    this.isVerified = false;
    this.userStatus = 'ACTIVE';
    this.longCreateAt = null;
    this.longVerifiedAt = null;
    this.longModifiedAt = null;
    this.strPhotoURL = null;
    this.strPhotoKTP = null;
  }
}
