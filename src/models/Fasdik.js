import Facility from './Facility';

export default class Fasdik extends Facility {
  constructor (strNamaLembaga, strJenisLembaga, kerusakan, reruntuhan) {
    super(kerusakan, reruntuhan);
    this.strNamaLembaga = strNamaLembaga;
    this.strJenisLembaga = strJenisLembaga;
    this.strJenjangPendidikan = null;
  }
}