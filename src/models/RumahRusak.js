import Facility from './Facility';
export class RumahRusak extends Facility {
  constructor (nmPemilik, nikPemilik, kerusakan, reruntuhan) {
    super(kerusakan, reruntuhan);
    this.nmPemilik = nmPemilik;
    this.nikPemilik = nikPemilik;
  }
}
