import Facility from './Facility';
export default class FasilitasUmum extends Facility {
  constructor (strNmFasilitas, kerusakan, reruntuhan) {
    super(kerusakan, reruntuhan);
    this.intKategori = null;
    this.strNmFasilitas = null;
  }
}